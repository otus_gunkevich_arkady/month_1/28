﻿using ConsoleMenu;
using ConsoleMenu.Components;
using System;
using System.Diagnostics;

namespace DotnetDev.Homework._28
{
    public class Start : Menu
    {
        private Dictionary<string, double> _table;
        private int _count;
        private int _minCount;
        private int _maxCount;
        private int[] _arr;
        #region Menu
        public Start()
        {
            _minCount = 100000;
            _maxCount = _minCount * 10000;
            _count = _minCount;
            UpdateButton();
            UpdateText();
        }
        public override void Left()
        {
            _count = Math.Max(_count / 10, _minCount);
            UpdateText();
        }
        public override void Right()
        {
            _count = Math.Min(_count * 10, _maxCount);
            UpdateText();
        }
        private void UpdateButton()
        {
            _buttons = new List<Button>
            {
                new Button("Получить полный ответ (полный)", async () => await VisualFull()),
                new Button("[*] Выйти", () => _exit = true)
            };
        }
        private void UpdateText()
        {
            _text = new List<string>() { $"Количество элементов  << {_count.ToString("### ### ### ### ##0")}/{_maxCount.ToString("### ### ### ### ##0")} >>" };
            if (_table?.Count > 0)
            {
                _text.AddRange(new Table().GetDouble(_table, 1, "Описание", "Время в секундах (по убыв)"));
            }
            _text.AddRange(new string[]
                    {
                        "",
                        "",
                        "Гункевич Аркадий Игоревич | OTUS",
                        "ДЗ: Внутрипроцессное взаимодействие"
                    });
        }
        #endregion
        private void LoadArray() => _arr = _count.GetArrayCountFill();
        private async Task VisualFull()
        {
            Console.Clear();
            Console.WriteLine("Идёт выполнение...");
            Stopwatch stopwatch = Stopwatch.StartNew();
            LoadArray();
            _table = new Dictionary<string, double>();
            int sum = 0;
            int countThread = 10;
            double secondTotal = 0;
            this._isBlock = true;

            stopwatch = Stopwatch.StartNew();
            _arr.Sum();
            stopwatch.Stop();
            _table.Add($"Linq-Sum", new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds);
            secondTotal += new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds;

            stopwatch = Stopwatch.StartNew();
            _arr.SumDefault();
            stopwatch.Stop();
            _table.Add($"SumDefault", new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds);
            secondTotal += new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds;

            stopwatch = Stopwatch.StartNew();
            _arr.SumArrayForEach();
            stopwatch.Stop();
            _table.Add($"SumArrayForEach", new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds);
            secondTotal += new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds;

            countThread = Process.GetCurrentProcess().Threads.Count;
            stopwatch = Stopwatch.StartNew();
            sum = await _arr.SumParallelThread(countThread);
            stopwatch.Stop();
            _table.Add($"SumParallelThread (потоков {countThread})", new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds);
            secondTotal += new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds;

            _table = _table.OrderBy(s => s.Value).ToDictionary(s => s.Key, s => s.Value);
            _table.Add("Всего", secondTotal);
            UpdateText();
            Write();
            this._isBlock = false;
        }
    }
}
