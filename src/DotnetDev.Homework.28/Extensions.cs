﻿namespace DotnetDev.Homework._28
{
    public static class Extensions
    {
        public static int[] GetArrayCountFill(this int count)
        {
            var result = new int[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = 1;
            }
            return result;
        }
        public static int SumDefault(this int[] arr)
        {
            int result = 0;
            for (int i = 0; i < arr.Length; i++)
                result += arr[i];
            return result;
        }
        public static int SumDefault(this int[] arr, int start, int take)
        {
            int result = 0;
            take = Math.Min(arr.Length, start + take);
            for (int i = start; i < take; i++)
                result += arr[i];
            return result;
        }
        public static int SumArrayForEach(this int[] arr)
        {
            int result = 0;
            Array.ForEach(arr, s => { result += s; });
            return result;
        }
        //public static int SumAggregate(this int[] arr) => arr.Aggregate((a, b) => a + b);
        public static async Task<int> SumParallelThread(this int[] arr, int countThread)
        {
            var tasks = new List<Task<int>>();
            int stepSkip = arr.Count() / countThread;
            for (int i = 0; i < countThread; i++)
            {
                int start = stepSkip * i;
                tasks.Add(Task.Run(() => arr.SumDefault(start, stepSkip)));
            }
            int[] results = await Task.WhenAll(tasks);
            return results.SumDefault();
        }
    }
}
