﻿namespace ConsoleMenu
{
    public class Core
    {
        public List<Button> Buttons => _buttons;
        private List<Button> _buttons { get; set; }
        public List<string> Text => _text;
        private List<string> _text { get; set; }
        private int _changeIndex { get; set; }
        public void LoadButton(List<Button> buttons)
        {
            _buttons = buttons;
            _buttons.All(s => s.IsSelect = false);
            buttons.First().IsSelect = true;
        }
        public void LoadText(List<string> text)
        {
            _text = text;
        }
        public void ExecuteButton()
        {
            _buttons.FirstOrDefault(s => s.IsSelect)?.Execute();
        }
        public void UpSelect()
        {
            if (_changeIndex > 0)
            {
                _buttons[_changeIndex - 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
            SelectIndex();
        }
        public void DownSelect()
        {
            if (_changeIndex < _buttons.Count() - 1)
            {
                _buttons[_changeIndex + 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
            SelectIndex();
        }
        private void SelectIndex()
        {
            _changeIndex = _buttons.IndexOf(_buttons.FirstOrDefault(s => s.IsSelect));
        }
    }
}
