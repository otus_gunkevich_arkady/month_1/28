﻿namespace ConsoleMenu.Components
{
    public static class Extensions
    {
        public static string Repeat(this string value, int count)
        {
            try
            {
                return string.Concat(Enumerable.Repeat(value, count));
            }
            catch
            {
                return value;
            }
        }
    }
}
